import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
const instructions = Platform.select({
  ios: `Press Cmd+R to reload,\nCmd+D or shake for dev menu`,
  android: `Double tap R on your keyboard to reload,\nShake or press menu button for dev menu`,
});

export default class App extends Component {

  constructor(props) {  
    super(props);  
    this.state = {x: 0, y: 0, total :'', lineList : []};  
}  

  check = () => {
    if(this.state.x<0) {
      Alert.alert("X is less than 0")
    }
    else if(this.state.y>10){
      Alert.alert("Y is more than 10")
    }
    else{
      let t=this.state.x + this.state.y
      this.setState({total:"Total is : " + t})
      let l = []
      for(let i =0;i<t;i++){
        l.push(<View
          style={{
            width :200,
            borderBottomColor: 'black',
            borderBottomWidth: 5,
            marginTop:10
          }}
        />)
      }

      this.setState({lineList : l})

    }
    
  }
  render(){
  return (
    <View style={styles.container}>
      <View style={{flexDirection:"row"}}>
      <TextInput 
      placeholder="Enter X"
      keyboardType="numeric"
      style={styles.welcome}      
      onChangeText={(x) => this.setState({x : parseInt(x)})}  
      />

      <TextInput 
      placeholder="Enter Y"
      keyboardType="numeric"
      style={styles.welcome}      
      onChangeText={(y) => this.setState({y : parseInt(y)})}  
     />

</View>

<Text  style={{fontSize: 20,}}>{this.state.total}</Text>

{this.state.lineList}
     <TouchableOpacity onPress={this.check} style={styles.instructions}>
       <Text style={{alignSelf:"center", fontSize:30, marginTop: 20}}>Draw !</Text>
     </TouchableOpacity>
    </View>
  );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 100,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    height: 100,
    width: 200,
    marginTop : 40,
    borderRadius: 10,
    borderWidth : 2,
    backgroundColor: '#6666ff',
    marginBottom: 5,
  },
});
